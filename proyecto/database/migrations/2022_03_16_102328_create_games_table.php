<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamesTable extends Migration
{
    // Game(game_id, player_id, stats, num_jornada_global(dia++), fecha) 
    // // stats = puntos - asistencias - rebDef - rebOf - tapones - robos - turnovers - faltas - minutos - per
    // // Pertenecen a 1 Player

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('games');

        Schema::create('games', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->id();
            // $table->unsignedBigInteger('player_id');

            $table->tinyInteger('points')->nullable();
            $table->string('min')->nullable();

            $table->foreignId('id_jornada')->constrained('jornadas');
            $table->foreignId('player_id')->constrained('players');

            $table->tinyInteger('fgm')->nullable();
            $table->tinyInteger('fga')->nullable();
            $table->string('fgp')->nullable();

            $table->tinyInteger('ftm')->nullable();
            $table->tinyInteger('fta')->nullable();
            $table->string('ftp')->nullable();

            $table->tinyInteger('tpm')->nullable();
            $table->tinyInteger('tpa')->nullable();
            $table->string('tpp')->nullable();

            $table->tinyInteger('offReb')->nullable();
            $table->tinyInteger('defReb')->nullable();
            $table->tinyInteger('totReb')->nullable();
            $table->tinyInteger('assists')->nullable();
            $table->tinyInteger('pFouls')->nullable();
            $table->tinyInteger('steals')->nullable();
            $table->tinyInteger('turnovers')->nullable();
            $table->tinyInteger('blocks')->nullable();
            $table->tinyInteger('plusMinus')->nullable();
            $table->string('comment')->nullable();

            $table->date('fecha');

            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
