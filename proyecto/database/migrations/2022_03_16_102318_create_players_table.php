<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlayersTable extends Migration
{
    // Player (player_id, nombre, posicion, equipo_real, valor_mercado, puntos-fantasia-jornada, participaciones, participaciones_ayer, id_api_externa)

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->id();
            $table->string('firstname');
            $table->string('lastname');
            $table->string('posicion');
            $table->string('image_url');
            // 1(G) - 2(SG) - 3(SF) - 4(PF) - 5(C)
            // $table->integer('equipo_real');
            $table->float('valor_mercado')->default(0);
            $table->float('puntos_fantasia_jornada')->default(0);
            $table->integer('participaciones')->default(0);
            $table->integer('participaciones_ayer')->default(0);
            $table->bigInteger('id_api_api_nba')->index();
            $table->integer('id_api_balldontlie');
            $table->integer('id_api_individual_stats');
            $table->float('altura')->nullable();
            $table->float('peso')->nullable();
            // $table->date('fecha');
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players');
    }
}
