<?php

namespace Database\Seeders;

use App\Models\League;
use App\Models\Team;
use App\Models\LeagueTeam;
use App\Models\PlayerTeam;
use App\Models\User;
use GuzzleHttp\Promise\Create;
use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        User::factory(20)->create()->each(function ($user) {
            // VALORES
            $ligaValueRand = rand(0, 2);
            $equipoValueRand = rand(1, 3);

            // CREAMOS
            League::factory($ligaValueRand)->create(['user_id' => $user->id]);


            Team::factory($equipoValueRand)->create(['user_id' => $user->id])->each(function ($team) {
                PlayerTeam::factory(rand(5, 12))->create(['team_id' => $team->id]);
                // PlayerTeam::factory(rand(5, 12))->create(['team_id' => $team->id, 'player_id' => rand(0, 600)]);
            });
        });
    }
}
