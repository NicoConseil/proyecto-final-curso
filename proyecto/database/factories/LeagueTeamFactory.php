<?php

namespace Database\Factories;

use App\Models\LeagueTeam;
use Illuminate\Database\Eloquent\Factories\Factory;
use Ramsey\Uuid\Type\Integer;

class LeagueTeamFactory extends Factory
{
    // league-team (id, league_id, team_id, puntos_totales, puntos_ultima_jornada, total_jornadas)
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = LeagueTeam::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'league_id' => $this->faker->numberBetween(1, 10),
            'team_id' => $this->faker->numberBetween(1, 10),
            'puntos_totales' => $this->faker->numberBetween(100, 200),
            'puntos_ultima_jornada' => 0,
            'total_jornadas' => 0,
        ];
    }
}
