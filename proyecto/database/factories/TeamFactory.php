<?php

namespace Database\Factories;

use App\Models\Team;
use Illuminate\Database\Eloquent\Factories\Factory;

class TeamFactory extends Factory
{
    // Team (team_id, user_id, nombre, flag_12_jugadores, dinero_disponible)
    // // Tienen JUGADORES (4G-4F-2C-2Extras-> Sino penalizacion por roster sin recambios)
    // // Pertenecen a LIGAS

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Team::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => $this->faker->numberBetween(1, 10),
            'nombre' => $this->faker->colorName(),
            'flag_12_jugadores' => $this->faker->boolean(60),
            'dinero_disponible' => $this->faker->numberBetween(1000, 10000),
        ];
    }
}
