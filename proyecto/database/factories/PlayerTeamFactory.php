<?php

namespace Database\Factories;

use App\Models\PlayerTeam;
use Illuminate\Database\Eloquent\Factories\Factory;

class PlayerTeamFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PlayerTeam::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'player_id' => $this->faker->numberBetween(1, 600),
            'team_id' => $this->faker->numberBetween(1, 10),
        ];
    }
}
