<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GameController;
use App\Http\Controllers\TeamController;
use App\Http\Controllers\LeagueController;
use App\Http\Controllers\PlayerController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('/test',  function () {
//     return 'CHEKIANDO API';
// });

// Route::middleware('auth:sanctum')->group(function () {
Route::middleware('auth:sanctum')->group(function () {
    // User
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    // PLAYERS
    // Route::get('/players', [PlayerController::class, 'index'])->name('players.index');
    // Route::get('/player/{id}', [PlayerController::class, 'show'])->whereNumber('id');
    // Route::get('/players/{id}', [PlayerController::class, 'show'])->whereNumber('id')->name('players.show');

    // // GAMES
    // Route::get('/games', [GameController::class, 'index'])->name('games.index');
    // Route::get('/game/{id}', [GameController::class, 'show'])->whereNumber('id');
    // Route::get('/games/{id}', [GameController::class, 'show'])->whereNumber('id')->name('games.show');

    // // LEAGUES
    // Route::get('/leagues', [LeagueController::class, 'index'])->name('leagues.index');
    // Route::get('/league/{id}', [LeagueController::class, 'show'])->whereNumber('id');
    // Route::get('/leagues/{id}', [LeagueController::class, 'show'])->whereNumber('id')->name('leagues.show');

    // Route::post('/league', [LeagueController::class, 'store']);
    // Route::post('/leagues', [LeagueController::class, 'store'])->name('leagues.store');

    // Route::put('/league/{id}', [LeagueController::class, 'update'])->whereNumber('id');
    // Route::put('/leagues/{id}', [LeagueController::class, 'update'])->whereNumber('id')->name('leagues.update');

    // Route::delete('/league/{id}', [LeagueController::class, 'destroy'])->whereNumber('id');
    // Route::delete('/leagues/{id}', [LeagueController::class, 'destroy'])->whereNumber('id')->name('leagues.destroy');

    // // TEAMS
    // Route::get('/teams', [TeamController::class, 'index'])->name('teams.index');
    // Route::get('/team/{id}', [TeamController::class, 'show'])->whereNumber('id');
    // Route::get('/teams/{id}', [TeamController::class, 'show'])->whereNumber('id')->name('teams.show');

    // Route::post('/team', [TeamController::class, 'store']);
    // Route::post('/teams', [TeamController::class, 'store'])->name('teams.store');

    // Route::put('/team/{id}', [TeamController::class, 'update'])->whereNumber('id');
    // Route::put('/teams/{id}', [TeamController::class, 'update'])->whereNumber('id')->name('teams.update');

    // Route::delete('/team/{id}', [TeamController::class, 'destroy'])->whereNumber('id');
    // Route::delete('/teams/{id}', [TeamController::class, 'destroy'])->whereNumber('id')->name('teams.destroy');
});
