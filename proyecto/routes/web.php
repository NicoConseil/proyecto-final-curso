<?php

use App\Http\Controllers\LeagueController;
use App\Http\Controllers\LeagueTeamController;
use App\Http\Controllers\PlayerController;
use App\Http\Controllers\TeamController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Index');
})->name('inicio');

Route::get('/inicio', function () {
    return Inertia::render('Index');
});
Route::get('/nosotros', function () {
    return Inertia::render('Nosotros');
})->name('nosotros');
Route::get('/reglas', function () {
    return Inertia::render('Reglas');
})->name('reglas');

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {

    // MERCADO
    Route::prefix('mercado')->controller(PlayerController::class)->group(function () {
        Route::get('/', 'index')->name('mercado.index');
        Route::get('search', 'search')->name('mercado.search');
        Route::get('{player}', 'show')->name('mercado.show');
    });
    // GESTION
    Route::prefix('gestion')->group(function () {

        // INDEX DASHBOARD
        // Route::get('/', function () {
        //     return Inertia::render('Gestion', [
        //         'equipos' => 'lolol',
        //         'ligas' => 'ligas',
        //     ]);
        // })->name('dashboard');

        // LIGAS
        Route::prefix('ligas')->controller(LeagueController::class)->group(function () {
            Route::get('/', 'index')->name('ligas.index');
            Route::get('{league}', 'show')->name('ligas.show');
            // Route::get('crear', 'create')->name('ligas.create');
            // Route::get('editar/{league}', 'edit'); Se edita desde modal

            Route::post('crear', 'store')->name('ligas.store');
            Route::put('editar/{league}', 'update')->name('ligas.update');
            Route::delete('borrar/{league}', 'destroy')->name('ligas.destroy');
        });

        // EQUIPOS
        Route::prefix('equipos')->controller(TeamController::class)->group(function () {
            Route::get('/', 'index')->name('equipos.index');
            Route::get('crear', 'create')->name('equipos.create');
            Route::get('plantilla/{team}', 'editPlantilla')->name('equipos.edit.plantilla');
            // Route::get('ligas/{team}', 'updateLigas')->name('equipos.update.ligas');
            Route::get('{team}', 'show')->name('equipos.show');

            Route::post('crear', 'store')->name('equipos.store');
            Route::put('editar/{team}', 'update')->name('equipos.update');
            Route::delete('borrar/{team}', 'destroy')->name('equipos.destroy');
            Route::patch('plantilla/{team}', 'updatePlantilla')->name('equipos.patch.plantilla');
        });

        // LIGA - EQUIPO
        Route::prefix('union')->controller(LeagueTeamController::class)->group(function () {
            Route::post('crear/{team}/{league}', 'store')->name('union.create');
            Route::delete('borrar/{team}/{league}', 'destroy')->name('union.destroy');
        });
    });
});
