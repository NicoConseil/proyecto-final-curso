/
/index
/reglas
/nosotros

-- GUESTS
/login
/register

-- USERS
/gestion (dashboard)
/gestion/equipos (dashboard - equipos)
/gestion/equipo/:id (edit - equipo)
/gestion/ligas (dashboard - ligas)
/gestion/liga/:id (edit - liga)

/ver/equipo/:id (ver - equipo)
/ver/liga/:id (ver - liga)