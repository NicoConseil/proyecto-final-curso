<?php

namespace App\Http\Requests;

use App\Rules\ArrayIdsPlayersIntegers;
use Illuminate\Foundation\Http\FormRequest;

class TeamAnhadirPlayerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Jugadores
            'players' => ['required', 'array', new ArrayIdsPlayersIntegers],
        ];
    }
}
