<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

// Team (team_id, user_id, nombre, flag_12_jugadores, dinero_disponible)
class TeamCreateStoreRequest extends FormRequest
{
    /**
     * Indicates if the validator should stop on the first rule failure.
     *
     * @var bool
     */
    protected $stopOnFirstFailure = true;

    /**
     * The URI that users should be redirected to if validation fails.
     *
     * @var string
     */
    protected $redirect = '';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // ID del Usuario
            'user_id' => 'required|integer',
            // Nombre del equipo
            'nombre' => 'required|string',
        ];
    }
}
