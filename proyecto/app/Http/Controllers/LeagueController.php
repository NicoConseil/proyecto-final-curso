<?php

namespace App\Http\Controllers;

use App\Http\Requests\LeagueStoreRequest;
use App\Http\Requests\LeagueUpdateRequest;
use App\Models\League;
use Error;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class LeagueController extends Controller
{
    // Cada usuario puede ver las ligas/liga, crearla, editarla y eliminarla
    // League (league_id, user_id, nombre, num_jornadas)
    // Pertenecen a USERS
    // Tienen EQUIPOS

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userLeagues = $request->user()->leagues()->get();
        return Inertia::render('Leagues/Index', ['ligas' => $userLeagues]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, League $league)
    {
        $user = $request->user();
        $propiedad = $user->id == $league->user_id;
        if (!$propiedad) {
            $propietario = $league->user()->get()[0];
        } else {
            $propietario = $user;
        }
        $equipos = $league->teams()->get();
        return Inertia::render('Leagues/Show', ['liga' => $league, 'equipos' => $equipos, 'propiedad' => $propiedad, 'propietario' => $propietario]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // League (league_id, user_id, nombre, num_jornadas = 0)
        $user = $request->user();
        $request->validate([
            'nombre' => 'required|string'
        ]);
        $input = $request->all();
        $input['user_id'] = $user->id;
        try {
            $league = new League($input);
            $league->save();
            return Redirect::route('ligas.show', ['league' => $league->id]);
        } catch (\Throwable $er) {
            return Redirect::route('ligas.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  League $league
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, League $league)
    {
        // League (league_id, user_id, nombre, num_jornadas = 0)
        $user = $request->user();
        $request->validate([
            'nombre' => 'required|string'
        ]);
        if ($user->id == $league->user_id) {
            $league->update($request->all());
            return Redirect::route('ligas.show', ['league' => $league->id]);
        } else {
            return Redirect::route('ligas.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  League $league
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, League $league)
    {
        // League (league_id, user_id, nombre, num_jornadas = 0)
        $user = $request->user();
        if ($user->id == $league->user_id) {
            $league->delete();
        }
        return Redirect::route('ligas.index');
    }
}
