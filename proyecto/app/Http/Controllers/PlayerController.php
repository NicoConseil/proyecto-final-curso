<?php

namespace App\Http\Controllers;

use Error;
use Exception;
use Inertia\Inertia;
use App\Models\Player;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\PlayerCollection;

class PlayerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Array
     */
    public function index(Request $request)
    {
        $request->validate([
            'page' => 'integer',
            'totPerPage' => 'integer',
            'posicion' => 'string',
            'lastname' => 'string',
            'firstname' => 'string',
        ]);
        $page = $request->input('page', 1);
        $tot = $request->input('totPerPage', 50);
        $firstname = $request->input('firstname', null);
        $lastname = $request->input('lastname', null);
        $posicion = $request->input('posicion', null);

        $query = Player::query();

        if (!is_null($firstname)) {
            $query = $query->where('firstname', 'LIKE', '%' . $firstname . '%');
        }
        if (!is_null($lastname)) {
            $query = $query->where('lastname', 'LIKE', '%' . $lastname . '%');
        }
        if (!is_null($posicion)) {
            $string = strval($posicion);
            if (preg_match('/(G|F|C)/', $string)) {
                $query = $query->where('posicion', 'LIKE', $string . '%');
            }
        }
        $query = $query->orderBy('valor_mercado', 'desc');

        $players = $query->paginate($tot, ['*'], 'page', $page);


        return Inertia::render('Players/Index', ['jugadores' => $players]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return App\Models\Player
     */
    public function show(Request $request, Player $player)
    {
        // $pointsAVG = DB::table('games')->select(DB::raw('round(AVG(points),0) as pointsAVG'))->groupBy('points')->get();
        $pointsAVG = DB::table('games')->avg('points');
        $rebAVG = DB::table('games')->avg('totReb');
        $astAVG = DB::table('games')->avg('assists');
        $games = $player->games()->get();

        return Inertia::render('Players/Show', ['jugador' => $player, 'games' => $games, 'pointsAVG' => $pointsAVG, 'rebAVG' => $rebAVG, 'astAVG' => $astAVG]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return App\Models\Player
     */
    public function search(Request $request)
    {
        $request->validate([
            'page' => 'integer',
            'totPerPage' => 'integer',
            'posicion' => 'string',
            'lastname' => 'string',
            'firstname' => 'string',
        ]);
        $page = $request->input('page', 1);
        $tot = $request->input('totPerPage', 50);
        $firstname = $request->input('firstname', null);
        $lastname = $request->input('lastname', null);
        $posicion = $request->input('posicion', null);

        $query = Player::query();

        if (!is_null($firstname)) {
            $query = $query->where('firstname', 'LIKE', '%' . $firstname . '%');
        }
        if (!is_null($lastname)) {
            $query = $query->where('lastname', 'LIKE', '%' . $lastname . '%');
        }
        if (!is_null($posicion)) {
            $string = strval($posicion);
            if (preg_match('/(G|F|C)/', $string)) {
                $query = $query->where('posicion', 'LIKE', $string . '%');
            }
        }

        $query = $query->orderBy('valor_mercado', 'desc');

        $players = $query->paginate($tot, ['*'], 'page', $page);

        return new Response($players, 200);
    }
}
