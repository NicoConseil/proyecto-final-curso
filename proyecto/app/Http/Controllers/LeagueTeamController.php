<?php

namespace App\Http\Controllers;

use App\Models\League;
use App\Models\Team;
use App\Models\LeagueTeam;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class LeagueTeamController extends Controller
{
    /**
     * Create and saves the resource
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Team $team, League $league)
    {
        $user = $request->user();
        if ($user->id == $team->user_id) {
            $union = new LeagueTeam(['team_id' => $team->id, 'league_id' => $league->id]);
            $union->save();
        }
        return Redirect::route('equipos.show', ['team' => $team->id]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Team $team, League $league)
    {
        $user = $request->user();
        $leagueTeam = LeagueTeam::query()->where('league_id', '=', $league->id)->where('team_id', '=', $team->id)->first();
        if ($user->id == $team->user_id) {
            $leagueTeam->delete();
        }
        return Redirect::route('equipos.show', ['team' => $team->id]);
    }
}
