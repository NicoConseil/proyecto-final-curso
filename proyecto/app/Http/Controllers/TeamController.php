<?php

namespace App\Http\Controllers;

use Error;
use Exception;
use App\Models\Team;
use Inertia\Inertia;
use App\Models\LeagueTeam;
use App\Models\PlayerTeam;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\TeamUpdateRequest;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\TeamCreateStoreRequest;
use App\Http\Requests\TeamAnhadirALigaRequest;
use App\Http\Requests\TeamAnhadirPlayerRequest;
use App\Models\League;
use App\Models\Player;

class TeamController extends Controller
{

    // Team (team_id, user_id, nombre, flag_12_jugadores, dinero_disponible)
    // // Tienen JUGADORES (4G-4F-2C-2Extras-> Sino penalizacion por roster sin recambios)
    // // Pertenecen a LIGAS

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $userTeams = $request->user()->teams()->get();
        // return $userTeams;
        $userTeams = $request->user()->teams()->get();
        return Inertia::render('Teams/Index', ['equipos' => $userTeams]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Team $team)
    {
        $user = $request->user();
        $propiedad = $user->id == $team->user_id;
        if (!$propiedad) {
            $propietario = $team->user()->get()[0];
        } else {
            $propietario = $user;
        }
        $ligas = $team->leagues()->get();
        $ligasAll = League::all();
        $jugadores = $team->players()->get();
        return Inertia::render('Teams/Show', ['equipo' => $team, 'jugadores' => $jugadores, 'ligas' => $ligas, 'propiedad' => $propiedad, 'propietario' => $propietario, 'ligasAll' => $ligasAll]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = $request->user();
        $teams = $user->teams()->get();
        $primero = false;
        if (sizeof($teams) == 0) {
            $primero = true;
        }
        $players = Player::orderBy('valor_mercado', 'desc')->get();
        $leagues = League::all();
        return Inertia::render('Teams/Create', ['jugadores' => $players, 'ligas' => $leagues, 'primero' => $primero]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\TeamCreateStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Team (team_id, user_id, nombre, flag_12_jugadores, dinero_disponible)
        $flagMal = false;
        $playerIds = [];
        $user = $request->user();
        $request->validate([
            'nombre' => 'required|string',
            'guards' => 'required|array|min:2|max:2',
            'forwards' => 'required|array|min:2|max:2',
            'centers' => 'required|array|min:1|max:1',
        ]);
        $input = $request->all();

        // CREAMOS EQUIPO
        $team = new Team(['nombre' => $input['nombre'], 'user_id' => $user->id, 'dinero_disponible' => 110, 'flag_12_jugadores' => false,]);
        // Asignamos dinero
        $money = $team->dinero_disponible;

        // GUARDS
        $guards = $input['guards'];
        foreach ($guards as $key => $value) {
            $guard = Player::find($value['id']);
            // Guardamos valores
            array_push($playerIds, $guard->id);

            // Calculamos
            $money -= $guard->valor_mercado;
        }

        // FORWARDS
        $forwards = $input['forwards'];
        foreach ($forwards as $key => $value) {
            $forward = Player::find($value['id']);
            // Guardamos valores
            array_push($playerIds, $forward->id);

            // Calculamos
            $money -= $forward->valor_mercado;
        }

        // CENTERS
        $centers = $input['centers'];
        foreach ($centers as $key => $value) {
            $center = Player::find($value['id']);
            // Guardamos valores
            array_push($playerIds, $center->id);

            // Calculamos
            $money -= $center->valor_mercado;
        }

        if ($money < 0) {
            $flagMal = true;
        }


        try {
            // INSERTAMOS SI TOCA
            if ($flagMal) {
                return Redirect::route('equipos.index');
            }

            // GUARDAMOS
            $team->dinero_disponible = $money;
            $team->save();
            foreach ($playerIds as $key => $id) {
                $union = new PlayerTeam(['player_id' => $id, 'team_id' => $team->id]);
                $union->save();
            }

            return Redirect::route('equipos.show', ['team' => $team->id]);
        } catch (\Throwable $er) {
            return Redirect::route('equipos.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\TeamUpdateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Team $team)
    {
        // Team (team_id, user_id, nombre, flag_12_jugadores, dinero_disponible)
        $user = $request->user();
        if ($user->id == $team->user_id) {
            $request->validate(['nombre' => 'required|string']);
            $team->update($request->all());
            return Redirect::route('equipos.show', ['team' => $team->id]);
        } else {
            return Redirect::route('equipos.index');
        }
    }

    /**
     * Show edit the roster from the team.
     *
     * @param  \Illuminate\Http\TeamUpdateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function editPlantilla(Request $request, Team $team)
    {
        $user = $request->user();

        if ($user->id == $team->user_id) {
            $plantilla = $team->players()->get();
            $query = Player::query();
            foreach ($plantilla as $key => $player) {
                $query = $query->where('id', '!=', $player['id']);
            }
            $players = $query->orderBy('valor_mercado', 'desc');
            $players = $query->get();

            $saldo = 0;
            foreach ($plantilla as $key => $player) {
                $saldo += $player->valor_mercado;
            }
            $saldo += $team->dinero_disponible;

            return Inertia::render('Teams/UpdateRoster', ['jugadores' => $players, 'plantilla' => $plantilla, 'equipo' => $team, 'saldoTotal' => $saldo]);
        }

        return Redirect::route('equipos.index');
    }
    /**
     * Update the roster from the team.
     *
     * @param  \Illuminate\Http\TeamUpdateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function updatePlantilla(Request $request, Team $team)
    {
        $user = $request->user();

        $request->validate([
            'newPlayers' => 'required|array|min:0|max:12',
            'deletedPlayers' => 'required|array|min:0|max:12',
        ]);
        $input = $request->all();
        $newPlayers = $input['newPlayers'];
        $deletedPlayers = $input['deletedPlayers'];

        $roster = $team->players()->get();
        $roster = $roster->toArray();

        $numPlayers = sizeof($roster) + sizeof($newPlayers) - sizeof($deletedPlayers);

        $idsToDelete = [];
        $idsToAdd = [];
        // SI el id corresponde y en total no pasan de 12 jugadores
        if ($user->id == $team->user_id && $numPlayers <= 12) {
            $flagMal = false;

            // Comprobamos que los ids existan y se puedan eliminar
            foreach ($deletedPlayers as $key => $value) {
                if (!in_array($value, $roster)) {
                    $flagMal = true;
                }
                array_push($idsToDelete, $value['id']);
            }

            // Comprobamos que los ids existan y se puedan añadir
            foreach ($newPlayers as $key => $value) {
                if (!Player::query()->where('id', '=', $value['id'])->exists()) {
                    $flagMal = true;
                }
                array_push($idsToAdd, $value['id']);
            }

            // Si dio algun fallo
            if ($flagMal) {
                return Redirect::route('equipos.index');
            }

            // Asignamos dinero
            $money = $team->dinero_disponible;

            // Quitamos
            foreach ($idsToDelete as $key => $id) {
                $player = Player::query()->where('id', '=', $id)->first();
                $money += $player->valor_mercado;
                $union = PlayerTeam::query()->where('player_id', '=', $id)->where('team_id', '=', $team->id)->first();
                $union->delete();
            }

            // Asignamos dinero y guardamos
            $team->dinero_disponible = $money;
            $team->save();

            // Creamos relaciones
            foreach ($idsToAdd as $key => $id) {
                $player = Player::query()->where('id', '=', $id)->first();
                $money -= $player->valor_mercado;
                if ($money > 0) {
                    $union = new PlayerTeam(['player_id' => $player->id, 'team_id' => $team->id]);
                    $union->save();
                } else {
                    $money += $player->valor_mercado;
                }
            }

            // Asignamos dinero y guardamos
            $team->dinero_disponible = $money;
            $team->save();

            return Redirect::route('equipos.show', ['team' => $team->id]);
        }

        return Redirect::route('equipos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Team $team)
    {
        $user = $request->user();
        if ($user->id == $team->user_id) {
            $team->delete();
        }
        return Redirect::route('equipos.index');
    }
}
