<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ArrayIdsPlayersIntegers implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!is_array($value)) {
            return false;
        }

        foreach ($value as $key => $idPlayer) {
            if (!is_integer($idPlayer)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'La introduccion de jugadores se hace mediante un array de integers.';
    }
}
