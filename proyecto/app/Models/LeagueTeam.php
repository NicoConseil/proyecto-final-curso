<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeagueTeam extends Model
{
    use HasFactory;
    // league-team (id, league_id, team_id, puntos_totales, puntos_ultima_jornada, total_jornadas)

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'league_team';

    protected $fillable = [
        'league_id',
        'team_id',
        'puntos_totales',
        'puntos_ultima_jornada',
        'total_jornadas',
    ];
}
