<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlayerTeam extends Model
{
    use HasFactory;
    // PLAYER-TEAM(id, player_id, team_id)
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'player_team';

    protected $fillable = ['player_id', 'team_id'];

    public $timestamps = false;
}
