<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class League extends Model
{
    use HasFactory;
    // League (league_id, user_id, nombre, num_jornadas)
    // // Pertenecen a USERS
    // // Tienen EQUIPOS

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'user_id',
        'nombre',
        'num_jornadas',
    ];

    // Cada Liga tiene varios equipos (tabala relacional de por medio)
    public function teams()
    {
        return $this->belongsToMany(Team::class, LeagueTeam::class);
    }
    // Cada liga tiene 1 creador 
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
