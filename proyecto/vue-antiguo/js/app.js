require('./bootstrap');

import { createApp } from 'vue';
import router from './router';
import store from './store';


import App from './app.vue'

const VUE_APP = createApp(App)
VUE_APP.use(store)
VUE_APP.use(router)
VUE_APP.mount('#main')