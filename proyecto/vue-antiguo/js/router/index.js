import { createRouter, createWebHistory } from 'vue-router'

import App from '../app'
// GENERALES
import Inicio from '../views/inicio'
import Nosotros from '../views/nosotros'
import Reglas from '../views/reglas'

// NO LOGUEADOS
import Login from '../views/guests/login'
import Resgitro from '../views/guests/registro'

// LOGUEADOS
import Gestion from '../views/users/gestion'


// Test component
import Prueba from '../components/prueba'

const routes = [{
        path: '/',
        component: Inicio
    },
    {
        path: '/inicio',
        component: Inicio
    },
    {
        path: '/nosotros',
        component: Nosotros
    },
    {
        path: '/reglas',
        component: Reglas
    },
    {
        path: '/registrarse',
        component: Resgitro
    },
    {
        path: '/login',
        component: Login
    },
    {
        path: '/gestion',
        component: Gestion
    },
    {
        path: '/gestion/equipos',
        component: Gestion
    },
    {
        path: '/gestion/ligas',
        component: Gestion
    },
];

export default createRouter({
    history: createWebHistory(),
    routes
})