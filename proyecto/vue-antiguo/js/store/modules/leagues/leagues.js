import router from './../../../router';

export default {
    namespaced: true,
    state() {
        return {
            league: null,
            leagues: null,
        }
    },
    mutations: {
        SET_LEAGUE(state, league) { state.league = league; },
        SET_LEAGUE(state, leagues) { state.leagues = leagues; },
    },
    actions: {
        async getLeagues({ commit, dispatch }, ) {
            console.log('Leagues')

            // await axios.get("/sanctum/csrf-cookie");
            axios.get("/api/leagues").then(res => {
                dispatch('handleStatus', res.status)
                return res
            }).catch(err => {
                return err
            })
        },

        async getLeague({ commit, dispatch }, id) {
            console.log('League')

            // await axios.get("/sanctum/csrf-cookie");
            axios.get(`/api/leagues/${id}`).then(res => {
                dispatch('handleStatus', res.status)
                return res
            }).catch(err => {
                return err
            })
        },

        async createLeague({ commit, dispatch }, form) {
            console.log('CREATE League')

            // await axios.get("/sanctum/csrf-cookie");
            axios.post(`/api/leagues`, form).then(res => {
                dispatch('handleStatus', res.status)
                return res
            }).catch(err => {
                return err
            })
        },

        async editLeague({ commit, dispatch }, id, form) {
            console.log('EDIT League')

            // await axios.get("/sanctum/csrf-cookie");
            axios.put(`/api/leagues/${id}`, form).then(res => {
                dispatch('handleStatus', res.status)
                return res
            }).catch(err => {
                return err
            })
        },

        async deleteLeague({ commit, dispatch }, id) {
            console.log('DELETE League')

            // await axios.get("/sanctum/csrf-cookie");
            axios.delete(`/api/leagues/${id}`).then(res => {
                dispatch('handleStatus', res.status)
                return res
            }).catch(err => {
                return err
            })
        },

        async handleStatus({ commit }, status) {
            if (status < 200 && status >= 300) {
                commit('SET_AUTH', false, { root: true })
                router.replace('/login')
            }
            commit('SET_CHARGE', false, { root: true })
        },
    },
    getters: {
        ['GET_LEAGUE']: state => state.leagues,
        ['GET_LEAGUES']: state => state.leagues,
    }
}