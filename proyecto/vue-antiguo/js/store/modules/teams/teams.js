import router from './../../../router';

export default {
    namespaced: true,
    state() {
        return {
            team: null,
            teams: null,
        }
    },
    mutations: {
        SET_TEAM(state, team) { state.team = team; },
        SET_TEAMS(state, teams) { state.teams = teams; },
    },
    actions: {
        async getTeams({ commit, dispatch }, ) {
            console.log('teams')

            commit('SET_CHARGE', true, { root: true })
                // await axios.get("/sanctum/csrf-cookie");
            axios.get("/api/teams").then(res => {
                dispatch('handleStatus', res.status)
                return res
            }).catch(err => {
                return err
            })
        },

        async getTeam({ commit, dispatch }, id) {
            console.log('team')

            commit('SET_CHARGE', true, { root: true })
                // await axios.get("/sanctum/csrf-cookie");
            axios.get(`/api/teams/${id}`).then(res => {
                dispatch('handleStatus', res.status)
                return res
            }).catch(err => {
                return err
            })
        },

        async createTeam({ commit, dispatch }, form) {
            console.log('CREATE team')

            commit('SET_CHARGE', true, { root: true })
                // await axios.get("/sanctum/csrf-cookie");
            axios.post(`/api/teams`, form).then(res => {
                dispatch('handleStatus', res.status)
                return res
            }).catch(err => {
                return err
            })
        },

        async editTeam({ commit, dispatch }, id, form) {
            console.log('EDIT team')

            commit('SET_CHARGE', true, { root: true })
                // await axios.get("/sanctum/csrf-cookie");
            axios.put(`/api/teams/${id}`, form).then(res => {
                dispatch('handleStatus', res.status)
                return res
            }).catch(err => {
                return err
            })
        },

        async deleteTeam({ commit, dispatch }, id) {
            console.log('DELETE team')

            commit('SET_CHARGE', true, { root: true })
                // await axios.get("/sanctum/csrf-cookie");
            axios.delete(`/api/teams/${id}`).then(res => {
                dispatch('handleStatus', res.status)
                return res
            }).catch(err => {
                return err
            })
        },

        async handleStatus({ commit }, status) {
            if (status < 200 && status >= 300) {
                commit('SET_AUTH', false, { root: true })
                router.replace('/login')
            }
            commit('SET_CHARGE', false, { root: true })
        },
    },
    getters: {
        ['GET_TEAM']: state => state.team,
        ['GET_TEAMS']: state => state.teams,
    }
}