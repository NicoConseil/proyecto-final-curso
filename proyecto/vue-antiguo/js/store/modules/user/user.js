export default {
    namespaced: true,
    state() {
        return {
            user: null,
        }
    },
    mutations: {
        SET_USER(state, user) { state.user = user; /* state.auth = Boolean(user); */ },
    },
    actions: {

        // Maneja el login
        async handleLogin({ commit, dispatch }, credentials) {
            console.log('login')
            commit('SET_CHARGE', true, { root: true })

            await axios.get("/sanctum/csrf-cookie");
            await axios.post("/auth/login", credentials)


            return dispatch('getUser');
        },

        // Maneja el logout
        async handleLogout({ commit, dispatch }) {
            console.log('logout')
            commit('SET_CHARGE', true, { root: true })

            await axios.get("/sanctum/csrf-cookie");
            await axios.post("/auth/logout")

            return dispatch('getUser');
        },

        // Maneja el registro
        async handleRegister({ commit, dispatch }, credentials) {
            console.log('register')
            commit('SET_CHARGE', true, { root: true })

            await axios.get("/sanctum/csrf-cookie");
            axios.post("/auth/register", credentials).then(res => {
                return dispatch('getUser');
            }).catch(err => {
                return err
            })
        },

        // Obtiene usuario
        async getUser({ commit, dispatch }) {
            await axios.get('/api/user')
                .then(res => {
                    commit('SET_USER', res.data)
                        // commit('SET_AUTH', true, { root: true })
                    dispatch('setAuth', true, { root: true })
                        // router.replace('/gestion');
                })
                .catch(err => {
                    dispatch('setAuth', false, { root: true })
                        // router.replace('/login');
                })
            dispatch('setCharge', false, { root: true })
        },
    },
    getters: {
        ['GET_USER']: state => state.user,
    }
}