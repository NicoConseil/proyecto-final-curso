// import Vue from 'vue';
import Vuex from 'vuex';
import User from './modules/user/user';
import Teams from './modules/teams/teams';
import Leagues from './modules/leagues/leagues';
import router from './../router'


// Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        user: User,
        teams: Teams,
        leagues: Leagues
    },
    state() {
        return {
            auth: false,
            charge: false
        }
    },
    mutations: {
        SET_AUTH(state, auth) { state.auth = auth; },
        SET_CHARGE(state, charge) { state.charge = charge; },
    },
    actions: {
        // FUNCION DE CONTROL DE AUTENTIFICADO Y REDIRECCION
        setAuth({ commit }, auth) {
            if (auth == true) {
                router.replace('/gestion');
            } else {
                router.replace('/login');
            }
            commit('SET_AUTH', auth)

        },
        // FUNCION PARA INDICAR QUE ESTAMOS CARGAQNDO ALGO
        setCharge({ commit }, charge) {
            commit('SET_CHARGE', charge)
        }
    },
    getters: {
        ['GET_AUTH']: state => state.auth,
        ['GET_CHARGE']: state => state.charge,
    }
})