import { jugTodos } from './../files/jugadores_definitivos_api_nba_API.js';
import { jugBallDontLie } from './../files/jugadores_balldonlie_API.js';
import { jugIndStats } from './../files/jugadores_individualstats.js';
import { Player } from './../modelos/Player.js'

let i = 0
let erroresBDL = []
let erroresIS = []
export async function insertPlayers() {
    for (let player of jugTodos) {

        let first = new RegExp(player.firstname + '.*')
        let last = new RegExp(player.lastname + '.*')
        let resultsBDL = await jugBallDontLie.filter(p1 => p1.first_name.match(first) && p1.last_name.match(last));
        let resultsIS = await jugIndStats.filter(p2 => p2.firstName.match(first) && p2.lastName.match(last));

        // console.log(player)
        // console.log(result)

        let resultBDL = await resultsBDL[0]
        if (!resultBDL) {
            erroresBDL.push(player)
            resultBDL = { id: 0 }
        }
        let resultIS = await resultsIS[0]
        if (!resultIS) {
            erroresIS.push(player)
            resultIS = { id: 0, headShotUrl: null }
        }
        let apiNBA = await parseInt(player.extern_id)
        let apiBDL = await parseInt(resultBDL.id)
        let apiIS = await parseInt(resultIS.id)
        let ms = parseFloat(player.meters);
        if (isNaN(ms)) ms = 0;
        let kgs = parseFloat(player.kilograms)
        if (isNaN(kgs)) kgs = 0;
        if (resultIS.headShotUrl == null) resultIS.headShotUrl = 'http://www.nba-league.local/storage/players/player_none.png';

        let insert = {
            firstname: player.firstname,
            lastname: player.lastname,
            posicion: player.pos,
            image_url: resultIS.headShotUrl,
            altura: ms,
            peso: kgs,
            id_api_api_nba: apiNBA,
            id_api_balldontlie: apiBDL,
            id_api_individual_stats: apiIS
        }

        try {
            await Player.create(insert);
            i++;
        } catch (err) {
            console.log(err)
        }

    };

    console.log('SIN FALLOS DE INSERCION: ' + i)
    console.log('TOTAL JUGADORES A INSERTAR: ' + jugTodos.length)

    // console.log('JUGADORES NO ENCONTRADOS EN BALLDONLIE: ' + erroresBDL)
    // console.log('TOT NO ENCONTRADOS: ' + erroresBDL.length)

    // console.log('JUGADORES NO ENCONTRADOS EN INDIVIDUAL STATS: ')
    // console.log(erroresIS)
    // console.log('TOT NO ENCONTRADOS: ' + erroresIS.length)
    console.log('JUGADORES MIGRADOS');


}

insertPlayers()