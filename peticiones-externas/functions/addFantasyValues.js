import { Game } from './../modelos/Games.js'
import { Player } from './../modelos/Player.js'
import { Jornada } from './../modelos/Jornada.js'
import { sequelize } from '../database/database.js';


// PUNTOS -> 8.4 => 1ps
// REBOTES -> 3.56 => 2ps
// ASISTENCIAS -> 1.84 => 3ps
// BLOCKS -> 0.38 => 4ps
// STEALS -> 0.63 => 4ps
// PLUSMINUS -> -10% <-> 10% Peq porcentaje de modificacion avg/2

// * 1punto == 0,365$

async function exec(idJornada) {

    let noWorkear = idJornada % 7;

    if (noWorkear) {
        return
    }

    let idsJornadas = [];
    for (let i = 0; i < 7; i++) {
        let value = idJornada - i;
        idsJornadas.push(value);
    }

    idsJornadas = idsJornadas.join(', ')

    let query = `SELECT player_id, AVG(points) as avgPoints, AVG(totReb) as avgRebounds, AVG(assists) as avgAssists, AVG(blocks) as avgBlocks, AVG(steals) as avgSteals, AVG(plusMinus) as avgPlusMinus FROM games WHERE id_jornada IN (${idsJornadas}) GROUP BY player_id`;

    const [results, metadata] = await sequelize.query(query);
    // console.log(results)
    // console.log(results.length)

    for (let avg of results) {
        let { avgPoints, avgRebounds, avgAssists, avgBlocks, avgSteals, avgPlusMinus, player_id } = avg
        // COMPROBAMOS
        if (isNaN(avgPoints)) avgPoints = 0;
        if (isNaN(avgRebounds)) avgRebounds = 0;
        if (isNaN(avgAssists)) avgAssists = 0;
        if (isNaN(avgBlocks)) avgBlocks = 0;
        if (isNaN(avgSteals)) avgSteals = 0;
        if (isNaN(avgPlusMinus)) avgPlusMinus = 0;
        // console.log(avgPoints)
        let fantasyPoints = parseFloat(avgPoints) + (parseFloat(avgRebounds) * 2) + (parseFloat(avgAssists) * 3) + (parseFloat(avgBlocks) * 4) + (parseFloat(avgSteals) * 4)

        if (isNaN(fantasyPoints) || fantasyPoints == null) {
            fantasyPoints = 0
        }

        // JUGADOR A MODIFICAR
        let player = await Player.findByPk(player_id)

        // PLUS MINUS
        let percentagePlusMinus = avgPlusMinus / 2

        if (percentagePlusMinus > 10) percentagePlusMinus = 10
        else if (percentagePlusMinus < -10) percentagePlusMinus = -10

        fantasyPoints = fantasyPoints * (1 + (percentagePlusMinus / 100))

        let precioJugador = fantasyPoints * 0.365;


        player.valor_mercado = precioJugador
        player.puntos_fantasia_jornada = fantasyPoints

        await player.save()

    }
}

exec(28)