import axios from 'axios'
import writeFile from './writeFile.js';
import dotenv from 'dotenv'
import { Game } from './../modelos/Games.js'
import { Player } from './../modelos/Player.js'
import { Jornada } from './../modelos/Jornada.js'
import { sequelize } from '../database/database.js';

let dot = dotenv.config()

let peticiones1min = 0;

async function peticionPartidosApiNba(day) {
    let key = await dot.parsed.MIX_RAPIDAPI_KEY;

    const options = {
        method: 'GET',
        url: 'https://api-nba-v1.p.rapidapi.com/games',
        params: { date: day },
        headers: {
            'X-RapidAPI-Host': 'api-nba-v1.p.rapidapi.com',
            'X-RapidAPI-Key': key
        }
    };

    try {
        let response = await axios.request(options)
        peticiones1min++
        // console.log(response.data);
        let data = await response.data
        return data;
    } catch (err) { console.log(err) }
    return -1;
}

async function peticionStatsPartidoApiNba(idPartido) {
    let key = await dot.parsed.MIX_RAPIDAPI_KEY;

    const options = {
        method: 'GET',
        url: 'https://api-nba-v1.p.rapidapi.com/players/statistics',
        params: { game: idPartido },
        headers: {
            'X-RapidAPI-Host': 'api-nba-v1.p.rapidapi.com',
            'X-RapidAPI-Key': key
        }
    };

    try {
        let response = await axios.request(options)
        peticiones1min++
        // console.log(response.data);
        let data = await response.data
        return data;
    } catch (err) { console.log(err) }
    return -1;
}

async function waiter() {
    console.log('10 pet per second sobrepasadas, 1min de descanso')
    setTimeout(() => {
        return 1;
    }, 70000)
}

let errores = []

export async function getNewJornada() {
    // JORNADA QUE TOCA (ultima insertada)
    let jornadas = await Jornada.findAll();

    // console.log(jornadas)

    let target = jornadas.length - 1;

    let jornada = jornadas[target];

    let id_jornada = await jornada.id
    let fecha = await jornada.fecha;

    // Creamos JORNADA NUEVA
    let dateOP = new Date(fecha);
    dateOP.setDate(dateOP.getDate() + 1);

    dateOP = dateOP.toISOString()
    dateOP = dateOP.split('T');
    dateOP = dateOP[0]
        // console.log(dateOP[0])

    jornada = await Jornada.create({ fecha: dateOP })

    id_jornada = await jornada.id
    fecha = await jornada.fecha


    // PETICION DE PARTIDOS
    if (peticiones1min >= 9) { await waiter; }
    let response = await peticionPartidosApiNba(fecha)
    if (response === -1) { return }

    let games = await response.response;
    // console.log(games)

    // POR CADA PARTIDO PEDIMOS
    for (let game of games) {
        // Si es nba
        if (game.league == 'standard') {
            let id = await game.id

            // PETICION DE STATS PARTIDO
            if (peticiones1min >= 9) { await waiter; }
            let response = await peticionStatsPartidoApiNba(id)
            if (response === -1) { return }
            let stats = await response.response;

            // ANHADIMOS CADA JUGADOR
            for (let stat of stats) {

                let firstname = await stat.player.firstname
                let lastname = await stat.player.lastname
                let api_nba_id = await stat.player.id

                // BUSCAMOS JUGADOR

                // SI NO corresponde el ID
                const [results, metadata] = await sequelize.query(`SELECT * FROM players WHERE id_api_api_nba LIKE '${api_nba_id}'`);
                // console.log(results)

                let player_id = await stat.player.id;
                if (results.length != 0) {
                    let result = await results[0]
                    player_id = await result.id
                } else {

                    firstname = firstname.split("'");
                    firstname = firstname.join("\\");
                    // PROBAMOS CORRESPONDENCIA DEL NOMBRE
                    const [results2, metadata] = await sequelize.query(`SELECT * FROM players WHERE firstname LIKE "%${firstname}%" && lastname LIKE "%${lastname}%"`);

                    if (results2.length != 0) {
                        let result2 = await results2[0]
                        player_id = await result2.id
                    } else {

                        // SI NADA ENCAJA CONTINUAMOS Y PASAMOS PAGINA
                        errores.push(stat)
                        continue;
                    }
                }


                // DATOS
                let points = await stat.points;
                let min = await stat.min;

                let fgm = await stat.fgm;
                let fga = await stat.fga;
                let fgp = await stat.fgp;

                let ftm = await stat.ftm;
                let fta = await stat.fta;
                let ftp = await stat.ftp;

                let tpm = await stat.tmp;
                let tpa = await stat.tpa;
                let tpp = await stat.tpp;

                let offReb = await stat.offReb;
                let defReb = await stat.defReb;
                let totReb = await stat.totReb;

                let assists = await stat.assists;
                let pFouls = await stat.pFouls;
                let steals = await stat.steals;
                let turnovers = await stat.turnovers;
                let blocks = await stat.blocks;

                let plusMinus = await stat.plusMinus;
                let comment = await stat.comment;


                let insert = {
                    player_id: player_id,
                    points: points,
                    min: min,
                    fgm: fgm,
                    fga: fga,
                    fgp: fgp,
                    ftm: ftm,
                    fta: fta,
                    ftp: ftp,
                    tpm: tpm,
                    tpa: tpa,
                    tpp: tpp,
                    offReb: offReb,
                    defReb: defReb,
                    totReb: totReb,
                    assists: assists,
                    pFouls: pFouls,
                    steals: steals,
                    turnovers: turnovers,
                    blocks: blocks,
                    plusMinus: plusMinus,
                    comment: comment,
                    id_jornada: id_jornada,
                    fecha: fecha
                }

                try {
                    await Game.create(insert)

                    // ACTUALIZAMOS JORNADA
                    jornada.stats_insertados++;
                    await jornada.save();
                } catch (err) {
                    if (err.name != 'SequelizeForeignKeyConstraintError') {
                        console.log(err);
                    }
                    // errores.push(insert);
                }

            }

            // ACTUALIZAMOS JORNADA
            jornada.partidos_insertados++;
            await jornada.save();
        }
    }

    console.log(errores)
    console.log('ERRORES DE PETICION: ' + errores.length)
}
export async function firstJornada() {
    // JORNADA QUE TOCA (ultima insertada)
    let jornadas = await Jornada.findAll();

    // console.log(jornadas)

    let target = jornadas.length - 1;

    let jornada = jornadas[target];

    let id_jornada = await jornada.id
    let fecha = await jornada.fecha;

    // PETICION DE PARTIDOS
    if (peticiones1min >= 9) { await waiter; }
    let response = await peticionPartidosApiNba(fecha)
    if (response === -1) { return }

    let games = await response.response;
    // console.log(games)

    // POR CADA PARTIDO PEDIMOS
    for (let game of games) {
        // Si es nba
        if (game.league == 'standard') {
            let id = await game.id

            // PETICION DE STATS PARTIDO
            if (peticiones1min >= 9) { await waiter; }
            let response = await peticionStatsPartidoApiNba(id)
            if (response === -1) { return }
            let stats = await response.response;

            // ANHADIMOS CADA JUGADOR
            for (let stat of stats) {

                let firstname = await stat.player.firstname
                let lastname = await stat.player.lastname
                let api_nba_id = await stat.player.id

                // BUSCAMOS JUGADOR


                // const [results, metadata] = await sequelize.query(`SELECT * FROM players WHERE firstname LIKE '%${firstname}%' && lastname LIKE '%${lastname}%'`);
                const [results, metadata] = await sequelize.query(`SELECT * FROM players WHERE id_api_api_nba LIKE '${api_nba_id}'`);
                // console.log(results)

                let player_id = await stat.player.id;
                if (results.length != 0) {
                    let result = await results[0]
                    player_id = await result.id
                } else {
                    errores.push(stat)
                    continue;
                }


                // DATOS
                let points = await stat.points;
                let min = await stat.min;

                let fgm = await stat.fgm;
                let fga = await stat.fga;
                let fgp = await stat.fgp;

                let ftm = await stat.ftm;
                let fta = await stat.fta;
                let ftp = await stat.ftp;

                let tpm = await stat.tmp;
                let tpa = await stat.tpa;
                let tpp = await stat.tpp;

                let offReb = await stat.offReb;
                let defReb = await stat.defReb;
                let totReb = await stat.totReb;

                let assists = await stat.assists;
                let pFouls = await stat.pFouls;
                let steals = await stat.steals;
                let turnovers = await stat.turnovers;
                let blocks = await stat.blocks;

                let plusMinus = await stat.plusMinus;
                let comment = await stat.comment;


                let insert = {
                    player_id: player_id,
                    points: points,
                    min: min,
                    fgm: fgm,
                    fga: fga,
                    fgp: fgp,
                    ftm: ftm,
                    fta: fta,
                    ftp: ftp,
                    tpm: tpm,
                    tpa: tpa,
                    tpp: tpp,
                    offReb: offReb,
                    defReb: defReb,
                    totReb: totReb,
                    assists: assists,
                    pFouls: pFouls,
                    steals: steals,
                    turnovers: turnovers,
                    blocks: blocks,
                    plusMinus: plusMinus,
                    comment: comment,
                    id_jornada: id_jornada,
                    fecha: fecha
                }

                try {
                    await Game.create(insert)

                    // ACTUALIZAMOS JORNADA
                    jornada.stats_insertados++;
                    await jornada.save();
                } catch (err) {
                    if (err.name != 'SequelizeForeignKeyConstraintError') {
                        console.log(err);
                    }
                    // errores.push(insert);
                }

            }

            // ACTUALIZAMOS JORNADA
            jornada.partidos_insertados++;
            await jornada.save();
        }
    }

    console.log(errores)
    console.log('ERRORES DE PETICION: ' + errores.length)
}

getNewJornada()