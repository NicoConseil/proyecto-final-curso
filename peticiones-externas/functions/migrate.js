import { insertPlayers } from './insertPlayers.js'
import { firstJornada, getNewJornada } from './peticionNuevaJornada.js'
import { Jornada } from './../modelos/Jornada.js'

async function exec() {
    await insertPlayers()

    let initNBA = '2021-10-20'

    await Jornada.create({ fecha: initNBA })

    await firstJornada();

    console.log('Esperamos 1min para obtener otra peticion de una nueva jornada')
    await setTimeout(() => {
        getNewJornada().then(() => {
            console.log('MIGRADO')
        })
    }, 70000)

}


exec()