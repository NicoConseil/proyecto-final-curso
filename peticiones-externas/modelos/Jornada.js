import { DataTypes } from 'sequelize';
import { sequelize } from '../database/database.js';

export const Jornada = sequelize.define('jornadas', {
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true,
    },
    fecha: { type: DataTypes.DATEONLY, defaultValue: DataTypes.NOW },
    partidos_insertados: { type: DataTypes.INTEGER, defaultValue: 0 },
    stats_insertados: { type: DataTypes.INTEGER, defaultValue: 0 },
}, { timestamps: true, createdAt: 'created_at', updatedAt: 'updated_at' });