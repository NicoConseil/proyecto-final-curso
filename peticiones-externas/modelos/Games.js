import { DataTypes } from 'sequelize';
import { sequelize } from '../database/database.js';

export const Game = sequelize.define('games', {
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true,
    },
    player_id: { type: DataTypes.BIGINT },
    points: { type: DataTypes.TINYINT, defaultValue: 0 },
    min: { type: DataTypes.STRING, defaultValue: '0' },

    fgm: { type: DataTypes.TINYINT, defaultValue: 0 },
    fga: { type: DataTypes.TINYINT, defaultValue: 0 },
    fgp: { type: DataTypes.STRING, defaultValue: '0' },

    ftm: { type: DataTypes.TINYINT, defaultValue: 0 },
    fta: { type: DataTypes.TINYINT, defaultValue: 0 },
    ftp: { type: DataTypes.STRING, defaultValue: '0' },

    tpm: { type: DataTypes.TINYINT, defaultValue: 0 },
    tpa: { type: DataTypes.TINYINT, defaultValue: 0 },
    tpp: { type: DataTypes.STRING, defaultValue: '0' },

    offReb: { type: DataTypes.TINYINT, defaultValue: 0 },
    defReb: { type: DataTypes.TINYINT, defaultValue: 0 },
    totReb: { type: DataTypes.TINYINT, defaultValue: 0 },

    assists: { type: DataTypes.TINYINT, defaultValue: 0 },
    pFouls: { type: DataTypes.TINYINT, defaultValue: 0 },
    steals: { type: DataTypes.TINYINT, defaultValue: 0 },
    turnovers: { type: DataTypes.TINYINT, defaultValue: 0 },
    blocks: { type: DataTypes.TINYINT, defaultValue: 0 },

    plusMinus: { type: DataTypes.STRING, defaultValue: null },
    comment: { type: DataTypes.STRING, defaultValue: null },

    id_jornada: { type: DataTypes.SMALLINT },
    fecha: { type: DataTypes.DATEONLY, defaultValue: DataTypes.NOW },
}, { timestamps: false });