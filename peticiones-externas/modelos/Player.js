import { DataTypes } from 'sequelize';
import { sequelize } from '../database/database.js';
import { Game } from './Games.js';

export const Player = sequelize.define('players', {
    id: { type: DataTypes.BIGINT, primaryKey: true, },
    firstname: { type: DataTypes.STRING },
    lastname: { type: DataTypes.STRING },
    posicion: { type: DataTypes.STRING },
    image_url: { type: DataTypes.STRING },
    valor_mercado: { type: DataTypes.FLOAT, defaultValue: 0 },
    puntos_fantasia_jornada: { type: DataTypes.FLOAT, defaultValue: 0 },
    participaciones: { type: DataTypes.INTEGER, defaultValue: 0 },
    participaciones_ayer: { type: DataTypes.INTEGER, defaultValue: 0 },
    id_api_api_nba: { type: DataTypes.INTEGER },
    id_api_balldontlie: { type: DataTypes.INTEGER },
    id_api_individual_stats: { type: DataTypes.INTEGER },
    altura: { type: DataTypes.FLOAT },
    peso: { type: DataTypes.FLOAT },
}, { timestamps: false });

Player.hasMany(Game, {
    foreignKey: 'player_id',
    sourceKey: 'id_api_api_nba'
})

Game.belongsTo(Player, {
    foreignKey: 'player_id',
    targetId: 'id_api_api_nba'
})