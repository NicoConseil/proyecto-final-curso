import { sequelize } from "./database/database.js";

import './modelos/Games.js';
import './modelos/Player.js';
import './modelos/Jornada.js';

async function exec() {
    try {
        // await sequelize.authenticate();
        await sequelize.sync();
        console.log('CONEXION ESTABLECIDA')
    } catch (err) {
        console.log(err)
    }
    return;
}


exec()