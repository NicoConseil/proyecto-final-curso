import Sequelize from "sequelize";
// require('dotenv').config()
import dotenv from 'dotenv'
let dot = dotenv.config()

// console.log(dot)

export const sequelize = new Sequelize(
    dot.parsed.DB_DATABASE, // NOMBRE DE BASE DE DATOS
    dot.parsed.DB_USERNAME, // USUARIO
    dot.parsed.DB_PASSWORD, { // CONTRASEÑA
        host: dot.parsed.DB_HOST, // HOST
        dialect: dot.parsed.DB_CONNECTION, // DIALECTO
        port: dot.parsed.DB_PORT, // PORT
    }
)