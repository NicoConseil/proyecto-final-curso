const axios = require("axios");

const options = {
    method: 'GET',
    url: 'https://api-nba-v1.p.rapidapi.com/teams',
    params: { conference: 'West' },
    headers: {
        'X-RapidAPI-Host': process.env('X_RAPID_HOST'),
        'X-RapidAPI-Key': process.env('X_API_KEY'),
    }
};

axios.request(options).then(function(response) {
    console.log(response.data);
}).catch(function(error) {
    console.error(error);
});

let east = [1, 2, 4, 5, 6, 7, 10, 15, 20, 21, 24, 26, 27, 38, 41]
let west = [8, 9, 11, 14, 16, 17, 19, 22, 23, 25, 28, 29, 30, 31, 40]