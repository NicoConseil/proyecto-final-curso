import axios from 'axios';
import writeFile from './writeFile.js';

// PETICION
async function peticion(perPage, page) {
    const options = {
        method: 'GET',
        url: 'https://nba-player-individual-stats.p.rapidapi.com/players',
        headers: {
            'X-RapidAPI-Host': 'nba-player-individual-stats.p.rapidapi.com',
            'X-RapidAPI-Key': 'fb2951aacemsh709690c14694464p19c997jsna9a19621b72f'
        }
    };

    let response = await axios.request(options)
    let data = await response.data
    console.log(data);
    return data;

}


// EJECUCION
async function exec() {
    let values = [];

    let players = await peticion();
    // response = await JSON.stringify(response);
    // let players = await response.data;

    for (let player of players) {
        let playerJSON = await JSON.stringify(player)
        values.push(playerJSON);
    }

    // response = await response.data.toString();
    writeFile('test.json', values.toString());
}

// RUN
exec();