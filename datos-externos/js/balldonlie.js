import axios from 'axios';
import writeFile from './writeFile.js';

// PETICION
async function peticionBallDonLie(perPage, page) {
    const options = {
        method: 'GET',
        url: 'https://www.balldontlie.io/api/v1/players',
        params: { per_page: perPage, page: page },
    };

    let response = await axios.request(options)
    let data = await response.data
    await console.log(data);
    return data;

}


// EJECUCION
async function exec() {
    let values = [];
    for (let i = 1; i < 39; i++) {
        let response = await peticionBallDonLie(100, i);
        // response = await JSON.stringify(response);
        let players = await response.data;

        for (let player of players) {
            let playerJSON = await JSON.stringify(player)
            values.push(playerJSON);
        }

    }
    // response = await response.data.toString();
    writeFile('test.json', values.toString());
}

// RUN
exec();