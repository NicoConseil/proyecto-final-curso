const options = {
    method: 'GET',
    headers: {
        'X-RapidAPI-Host': process.env('X_RAPID_HOST'),
        'X-RapidAPI-Key': process.env('X_API_KEY'),
    }
};

fetch('https://api-nba-v1.p.rapidapi.com/players?team=1&season=2021', options)
    .then(response => response.json())
    .then(response => console.log(response))
    .catch(err => console.error(err));

/*
const axios = require("axios");
const options = {
  method: 'GET',
  url: 'https://api-nba-v1.p.rapidapi.com/players',
  params: {team: '1', season: '2021'},
  headers: {
    'X-RapidAPI-Host': process.env('X_RAPID_HOST'),
        'X-RapidAPI-Key': process.env('X_API_KEY'),
  }
};

axios.request(options).then(function (response) {
    console.log(response.data);
}).catch(function (error) {
    console.error(error);
});
*/