// Player (player_id, nombre, posicion, equipo_real, valor_mercado, puntos-fantasia-jornada, participaciones, participaciones_ayer, id_api_externa) amhadidos altura(m) - peso(Kg)


let teams = [1, 2, 4, 5, 6, 7, 10, 15, 20, 21, 24, 26, 27, 38, 41, 8, 9, 11, 14, 16, 17, 19, 22, 23, 25, 28, 29, 30, 31, 40]

let text = ['INSERT INTO players (firstname, lastname, posicion, id_api_externa, altura, peso) VALUES']

async function convertTOsql() {
    let construction = []
    
    // teams.forEach(team_id => {
    // });

        // ARCHIVO NECESARIO
        // let json = require(`./../Equipos/Equipo${team_id}.json`)
        let json = require(`./test_v2.json`)

        // FN de trasformado
        json.forEach(jugador => {
                // if (jugador.leagues.standard && jugador.leagues.standard.active == true) {}
                    let data = `("${jugador.firstname}", "${jugador.lastname}", '${jugador.pos}', ${jugador.extern_id}, ${jugador.meters}, ${jugador.kilograms})`
                    construction.push(data)
                        // console.log(data)
                
            })
            // console.log(text.length);


    // UNIMOS
    // console.log(text.length);
    construction = await construction.join(', ');
    await text.push(construction)
        // console.log(text);
    text = await text.join('')
    return text;
}

async function fromTeamsToJSON() {
    let construction = [];
    let existingIds = [];

    teams.forEach(team_id => {
        // ARCHIVO NECESARIO
        let json = require(`./../Equipos/Equipo${team_id}.json`)

        // FN de trasformado
        json.response.forEach(jugador => {
            // SI ES ACTIVO
            if (jugador.leagues.standard && jugador.leagues.standard.active == true && !existingIds.includes(jugador.id)) {

                let data = {
                    firstname: jugador.firstname,
                    lastname: jugador.lastname,
                    pos: jugador.leagues.standard.pos,
                    team_id: jugador.team_id,
                    extern_id: jugador.id,
                    meters: jugador.height.meters,
                    kilograms: jugador.weight.kilograms,
                };
                existingIds.push(jugador.id)
                construction.push(JSON.stringify(data))
                // console.log(data)
                
            }
        })

    });

    let ret = await construction.join(', ');
    return '[' + ret + ']'
}


async function writeArchive() {
    var fs = require("fs");

    let data = await convertTOsql();

    fs.writeFile('test_v2.sql', data, function(err) {
        if (err) {
            console.log(err);
            return;
        }
        console.log(`LISTO`);
    });
}

// EJECUTAMOS
writeArchive()