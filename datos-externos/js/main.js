const axios = require("axios");

async function createJsonFromTeam(idTeam) {
    //Leer el archivo desde node.js
    var fs = require("fs");

    const options = {
        method: 'GET',
        url: 'https://api-nba-v1.p.rapidapi.com/players',
        params: { team: idTeam, season: '2021' },
        headers: {
            'X-RapidAPI-Host': process.env('X_RAPID_HOST'),
            'X-RapidAPI-Key': process.env('X_API_KEY'),
        }
    };


    let data = await axios.request(options)
    data = await data.data
    console.log('Equipo: ' + idTeam + ' - Obtenido')
        // console.log(data)

    const jsonstring = await `const team${idTeam} = ${JSON.stringify(data)}`;

    // Escribios el string en el archivo
    fs.writeFile('Equipo' + idTeam + '.js', jsonstring, function(err) {
        if (err) {
            console.log(err);
            return;
        }
        console.log(`Equipo${idTeam} escrito`);
    });
}

let id = 0;
let east = [1, 2, 4, 5, 6, 7, 10, 15, 20, 21, 24, 26, 27, 38, 41]
let west = [8, 9, 11, 14, 16, 17, 19, 22, 23, 25, 28, 29, 30, 31, 40]

var interval = setInterval(() => {
    if (id < 15) {
        createJsonFromTeam(west[id]);
        id++
    } else {
        console.log('ACABAO')
        clearInterval(interval)
    }
}, 10000);