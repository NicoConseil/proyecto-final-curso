# proyecto-final-curso

Este es el repositorio git de mi proyecto final del ciclo de DAW

## Instalación
Debes tener instalado:  
- Docker  
- Node

(Revisar siempre supuestos abajo)*  
Para realizar la instalacion debes:  

1 - Descargar el repositorio con el proyecto  
2 - Actualizar los archivos .env del proyecto (proyecto y peticiones_externas) (puedes inspirarte en los example o directamente copiarlos)  
3 - Te situas en la carpeta de desarrollo  
4 - Ejecutas 'npm run build'  
5 - Ejecutas 'npm run up'  
6 - Ejecutas 'npm run install-composer'  
7 - Ejecutas 'npm run install-npm'  
8 - Ejecutas 'npm run permisos'  
9 - Ejecutas 'npm run storage-link'  
10 - LISTO. Accedes a traves de la url  

## Supuestos
1. Si por algún casual no se carga la base de datos correctamente entrad en http://localhost:8081 y con las credenciales de phpmyadmin de envfile de docker ejecutad en la base de datos el archivo /datos-externos/data/nba-league_v2.sql.
2. Si da algún fallo durante la instalación podeis entrar en los containers y ejecutar los comandos correspondientes del package.json de la carpeta docker en el orden correspondiente según el arriba indicado. (en el fondo ese package.json solo está para simplificarlo todo).
3. Al utilizar un nginx alguna vez se puede dar un problema de permisos. dado el entrono en el que suelo trabajar es seguro acostumbro a hacer chmod 777 -R tanto al proyecto como a los archivos del container 'app'. No debería haber fallos pero por si acaso.
4. Si este container se levanta en el centro debe hacerse en _ubuntu_ y en _maquinas_ _virtuales_, acordandose claro de los permisos.